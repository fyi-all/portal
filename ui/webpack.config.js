const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

/* @TODO
* -add home, 404, auth, reg pages
* -add routing
* add redux
* add dev-tools at react
* */

module.exports = {
	context: path.resolve(__dirname, './src'),
	devServer: {
		contentBase: path.resolve(__dirname, './src'),
	},
	entry: {
		index: ['./index.js'],
	},
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, './dist'),
		publicPath: '/assets',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [/(node_modules|dist)/],
				use: [{
					loader: 'babel-loader',
					options: { presets: ['es2015', 'react'] },
				}],
			},
			{
				test: /\.(sass|scss)$/,
				use: [
					'style-loader',
					'css-loader',
					'sass-loader'
				],
			},
			{
				test: /\.css$/,
				loader:  ExtractTextPlugin.extract({
					loader: 'css-loader?importLoaders=1',
				}),
			},
		],
	},
	plugins: [
		new ExtractTextPlugin({
			filename: '[name].bundle.css',
			allChunks: true,
		}),
	],
};