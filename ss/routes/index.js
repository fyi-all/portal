const express = require('express');
const path = require('path');
const router = express.Router();
const usersCtrl = require('../ctrls/users-ctrl');

/** User req */
router.get('/getusers', usersCtrl.getUsers);
router.get('/removeusers', usersCtrl.removeUsers);
router.post('/adduser', usersCtrl.addUser);

router.post('/login', usersCtrl.login);
router.post('/logout', usersCtrl.logout);

module.exports = router;