const mg = require('mongoose');
const config = require('../config');

mg.Promise = global.Promise;//костыль

mg.connect(config.mongoose.uri);

module.exports = mg;