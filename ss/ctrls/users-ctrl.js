const userCtrl = {};
const bcrypt = require('bcryptjs');
const UsersSchem = require('../mongoose/models/UsersSchem');
const jwt = require('jsonwebtoken');
const expressJWT = require('express-jwt');
const express = require('express');
const app = express();
const config = require('../config');

app.use(expressJWT({secret: config.SECRET}));

userCtrl.addUser = (req, res, next) => {
	let {firstName, lastName, email, password} = req.body;

	new UsersSchem({
		firstName,
		lastName,
		email,
		password: bcrypt.hashSync(password, bcrypt.genSaltSync(10))
	})
		.save()
		.then(user => res.json(user))
		.catch(err => {
			console.log(err);
			next(err)
		});
};

userCtrl.getUsers = (req, res, next) => {
	UsersSchem.find({})
		.then(user => {
			res.json(user)
		})
		.catch(err => {
			console.error(err);
			next(err)
		});
};

userCtrl.removeUsers = (req, res, next) => {
	UsersSchem
		.remove({})
		.then(user => {
			res.json(user)
		})
		.catch(err => {
			console.error(err);
			next(err)
		});
};

userCtrl.login = (req, res, next) => {
	UsersSchem
		.findOne({email: req.body.email})
		.then(user => {
			if (!user) {
				/** @todo обработать ошибку правильно */
				res.end({message: 'Login Error, bad email'});
			} else if (!bcrypt.compareSync(req.body.password, user.password)) {
				/** @todo обработать ошибку правильно */
				res.end({message: 'Login Error, bad password'});
			} else {
				user.token = jwt.sign(user, SECRET, {expiresIn: 60 * 60 * 24});

				user.save((err, result) => {
					if (err) {
						console.log(err);
						return;
					}
				});

				res.json({
					user: {
						email: user.email,
						firstName: user.firstName,
						lastName: user.lastName,
						_id: user._id
					},
					token: user.token
				});
			}
		})
		.catch(err => {
			console.error(err);
			next(err);
		});
};

userCtrl.logout = (req, res, next) => {
	console.log(req.body);
	UsersSchem.findOne({token: req.body.token})
		.then(user => {
			user.token = null;
			user.save();
			res.json({message: 'Logout'})
		})
		.catch(err => {
			console.error(err);
			next(err);
		});
};

module.exports = userCtrl;