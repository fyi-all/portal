import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom'

import HomePage from '../pages/HomePage';
import AuthPage from '../pages/AuthPage';
import RegistrationPage from '../pages/RegistrationPage';
import NotFoundPage from '../pages/NotFoundPage';

export default class BodyContainer extends Component {
	render(){
		return(
			<div>
				<Switch>
					<Route exact path="/" component={HomePage}/>
					<Route path="/auth" component={AuthPage}/>
					<Route path="/reg" component={RegistrationPage}/>
					<Route component={NotFoundPage}/>
				</Switch>
			</div>
		)
	}
}

