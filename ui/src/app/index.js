import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import {Provider} from 'react-redux';
import {MemoryRouter} from 'react-router-dom'

import configureStore from './store'

ReactDOM.render(
	<Provider store={configureStore()}>
		<MemoryRouter>
			<App />
		</MemoryRouter>
	</Provider>,
	document.querySelector('#root')
);