import React from 'react';

import {BrowserRouter as Router} from 'react-router-dom'

import NavigationContainer from './containers/NavigationContainer';
import BodyContainer from './containers/BodyContainer';
import FooterContainer from './containers/FooterContainer';

export default class App extends React.Component {
	render() {
		return (
			<Router>
				<div>
					<NavigationContainer/>
					<BodyContainer/>
					<FooterContainer/>
				</div>
			</Router>
		)
	}
};