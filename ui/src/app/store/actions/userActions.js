import {USER_REG, USER_AUTH, USER_LOGOUT, USER_REG_ERR, USER_LOGIN_ERR} from '../constants/userConst';
import config from '../../config';

export function registrationUser(formBody) {
	return (dispatch) => {
		fetch(`${config.server}/adduser`, {
			method: 'post',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(formBody)
		})
			.then((res) => res.json())
			.then((data) => {
				if (data.err) {
					dispatch({
						type: USER_REG_ERR,
						payload: data.err
					})
				} else {
					dispatch({
						type: USER_REG,
						payload: data//obj user information
					});
				}
			})
			.catch((err) => {
				dispatch({
					type: USER_REG_ERR,
					payload: err
				})
			});
	}
}