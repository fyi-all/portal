const UsersSchem = require('../mongoose/models/UsersSchem');

module.exports = (req, res, next) => {
	if (req.headers.token == 'null') {
		res.json({err: 401, message: "You must be logged"});
		return;
	}

	UsersSchem
		.findOne({token: req.headers.token})
		.then(res => {
			next();
		})
		.catch(err => {
			console.error(err);
			res.json({err: 401, message: "You must be logged"})
		});
};