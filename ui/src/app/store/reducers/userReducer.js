import {USER_REG, USER_AUTH, USER_LOGOUT, USER_REG_ERR, USER_LOGIN_ERR} from '../constants/userConst'

const initialState = {};

export default function (state = initialState, action) {
	switch (action.type) {
		case USER_REG:
			return Object.assign({}, state, {
				userCredentials: action.payload
			});
		default:
			return state;
	}
}